import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import { babel } from '@rollup/plugin-babel'
import postcss from 'rollup-plugin-postcss';
import copy from 'rollup-plugin-copy'
import typescript from '@rollup/plugin-typescript';
import pkg from './package.json'

export default {
  input: './src/index.tsx', // 入口文件
  output: [
    { file: pkg.main, format: 'cjs', exports: 'auto' },
    { file: pkg.module, format: 'esm' }
  ],
  plugins: [
    resolve(),
    commonjs(),
    babel({
      babelHelpers: 'runtime',
      exclude: 'node_modules/**', // 只编译我们的源代码
      extensions: ['.js', '.jsx', '.ts', '.tsx', '.scss'],
    }),
    typescript(),
    postcss({
      extract: false, // css通过链接引入
      // use: ['less'] // 编译 sass
    }),
    copy({
      targets: [{ src: 'src/index.less', dest: 'dist' }]
    }),
  ],
  external: ['react', 'antd', '@ant-design/icons'] // 将[模块]视为外部依赖项
}
