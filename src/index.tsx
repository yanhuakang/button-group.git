import React from 'react'
import {
  Button,
  ConfigProvider,
  Dropdown,
  // DropDownProps,
  Menu,
  Space
} from 'antd'
import Icon, { MoreOutlined } from '@ant-design/icons'

declare const Placements: [
  'topLeft',
  'topCenter',
  'topRight',
  'bottomLeft',
  'bottomCenter',
  'bottomRight'
]
declare type Placement = typeof Placements[number]

export type ButtonGroupProps = Partial<{
  outerCount: string | number // 外部显示几个按钮，其余放在Dropdown中
  direction: 'row' | 'column' // icon 方向，默认横向
  icon: React.ComponentType // 右侧更多 icon
  iconStyle: React.CSSProperties // 右侧更多 icon 样式
  children: React.ReactNode
  trigger: ('click' | 'hover' | 'contextMenu')[]
  placement: Placement
  arrow: boolean
}>

// export type ButtonGroupProps = ButtonGroupFCProps & DropDownProps

const ButtonGroupFC = ({
  outerCount = 2,
  placement = 'bottomLeft',
  direction = 'row',
  arrow = false,
  trigger = ['click'],
  icon,
  iconStyle = {},
  ...props
}: ButtonGroupProps) => {
  let btnArr
  if (Array.isArray(props.children)) {
    btnArr = Array.from([...props.children]).filter((item) => !!item)
  } else {
    btnArr = Array.from([props.children]).filter((item) => !!item)
  }

  let showChildren = [...btnArr]
  let moreChildren = []

  const maxCount = Number(outerCount)

  if (btnArr.length > maxCount + 1) {
    showChildren = btnArr.slice(0, maxCount)
    moreChildren = btnArr.slice(maxCount, btnArr.length)
  }

  return (
    <>
      {!!showChildren.length && (
        <Space className="button-group__show-children">{showChildren}</Space>
      )}
      {!!moreChildren?.length && (
        // @ts-ignore
        <Dropdown
          trigger={trigger}
          arrow={arrow}
          placement={placement}
          overlayClassName="button-group__dropdown"
          overlay={
            <Menu>
              {moreChildren.map((item, index) => {
                if (item.type.displayName === 'Button') {
                  return (
                    <Menu.Item
                      key={item.key ? item.key : index}
                      disabled={item.props.disabled}
                      onClick={item.props.onClick}
                    >
                      <Button
                        {...item.props}
                        size="small"
                        type="link"
                        onClick={(e: MouseEvent) => e.preventDefault()}
                      >
                        {item.props.children}
                      </Button>
                    </Menu.Item>
                  )
                } else if (item.type.name === 'Divider') {
                  return <Menu.Divider key={item.key ? item.key : index} />
                } else {
                  return (
                    <Menu.Item key={index} disabled={item.props.disabled}>
                      {item}
                    </Menu.Item>
                  )
                }
              })}
            </Menu>
          }
        >
          <Icon
            className="button-group__trigger-icon"
            style={{
              fontSize: 22,
              verticalAlign: 'middle',
              marginLeft: 8,
              transform: direction === 'row' ? 'rotate(90deg)' : 'none',
              ...iconStyle
            }}
            component={icon || (MoreOutlined as React.ComponentType)}
          />
        </Dropdown>
      )}
    </>
  )
}

const ButtonGroup = (props: ButtonGroupProps) => (
  <ConfigProvider prefixCls="ant">
    <ButtonGroupFC {...props} />
  </ConfigProvider>
)

export default ButtonGroup
