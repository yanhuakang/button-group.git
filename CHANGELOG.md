# 2.0.0 (2022-10-10)


### ✨ Features | 新功能

* # 1.0.1（2021-11-04） ([b298d15](https://gitee.com/yanhuakang/button-group/commit/b298d15))
* # 1.0.1（2021-11-04） ([e3e4163](https://gitee.com/yanhuakang/button-group/commit/e3e4163))
* # 1.0.2（2021-11-04） ([094566d](https://gitee.com/yanhuakang/button-group/commit/094566d))
* # 1.0.2（2021-11-04） ([40b6f88](https://gitee.com/yanhuakang/button-group/commit/40b6f88))
* # 1.0.4（2021-11-08） ([a866828](https://gitee.com/yanhuakang/button-group/commit/a866828))
* build ([4700c38](https://gitee.com/yanhuakang/button-group/commit/4700c38))
* 优化、less ([7fa1683](https://gitee.com/yanhuakang/button-group/commit/7fa1683))
* 正式版本 ([470363c](https://gitee.com/yanhuakang/button-group/commit/470363c))
* 里程碑 ([ab44ddb](https://gitee.com/yanhuakang/button-group/commit/ab44ddb))


### 🐛 Bug Fixes | Bug 修复

* # 1.0.5（2021-11-10） ([8526732](https://gitee.com/yanhuakang/button-group/commit/8526732))
* # 1.0.5（2021-11-10） ([c4fcfc6](https://gitee.com/yanhuakang/button-group/commit/c4fcfc6))



