module.exports = {
  presets: [
    [
      '@babel/preset-env',
      {
        modules: false,
        useBuiltIns: 'usage',
        corejs: '3.25.0'
      }
    ],
    "@babel/preset-typescript",
    '@babel/preset-react'
  ],
  plugins: ['@babel/plugin-transform-runtime']
}
