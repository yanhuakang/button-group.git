# ButtonGroup



## 介绍

1. 本组件基于[React](https://react.docschina.org/)、[Ant Design](https://ant.design/index-cn)，适用于React + Ant Design项目
2. 本组件设计为超过一定数量的按钮将展示在`Dropdown`中

![sample](https://download.lietou-static.com/img/618c7bda6101044550e66bb002u.gif)

## 使用方式

### 安装

```js
npm i antd-button-group
```



### 使用

```css
@import '~antd/dist/antd.less'; // 引入antd样式
@import '~antd-button-group/dist/index.less';
```



```js
import ButtonGroup from 'antd-button-group'
```

```html

<ButtonGroup>
  <Button size='small' type='link' onClick={() => message.success('编辑')}>编辑</Button>
  <Button size='small' type='link' danger onClick={() => message.warning('删除')}>删除</Button>
  <Button size='small' type='link' onClick={() => message.success('随便什么操作')}>随便什么操作</Button>
  <Button type='primary' onClick={() => message.success('primary to link')}>primary to link</Button>
  <Button size='small' type='link' danger onClick={() => message.error('按钮 danger')}>按钮 danger</Button>
  <Divider />
  <a onClick={() => message.success('a标签')}>a标签</a>
</ButtonGroup>
```



## API

### Props

| 属性       | 说明                                 | 类型           | 默认值                                                       | 可选值                                                       |
| :--------- | :----------------------------------- | -------------- | :----------------------------------------------------------- | ------------------------------------------------------------ |
| outerCount | 外部显示几个按钮，其余放在Dropdown中 | Number、String | 2                                                            |                                                              |
| direction  | 内置icon的排列方向                   | String         | row（横向）                                                  | column                                                       |
| placement  | Dropdown弹出位置                     | String         | bottomLeft                                                   | `bottomLeft`、`bottomCenter`、<br />`bottomRight`<br />`topLeft`、`topCenter`、`topRight` |
| arrow      | Dropdown下拉框箭头是否显示           | Boolean        | false                                                        |                                                              |
| trigger    | 触发下拉的行为, 移动端不支持 hover   | Array          | ['click']                                                    | `click`、`hover`、`contextMenu`                              |
| icon       | 右侧更多 icon                        | ComponentType  | [Antd Icon](https://ant.design/components/icon-cn/) MoreOutlined |                                                              |
| iconStyle  | 右侧更多 icon 样式                   | CSSProperties  | { fontSize: 22,  verticalAlign: 'middle',  marginLeft: 2 }   |                                                              |
| children   | 按钮                                 | ReactNode      | -                                                            |                                                              |



### children

| PropTypes.node                                               | 说明                                                         |
| :----------------------------------------------------------- | :----------------------------------------------------------- |
| `<Button type='primary'>按钮</Button>`                       | `Dropdown`中的`Button`将会自动转为`<Button type='link'>按钮</Button>` |
| `<Menu.Divider />`                                           | 使用该组件渲染分割线                                         |
| `<Button type='primary' disabled>按钮</Button>`<br />`<a disabled onClick={() => message.success('a标签')}>a标签</a>` | `disabled`不可用                                             |

